<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SteamApi */

$this->title = Yii::t('app', 'Create Steam Api');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Steam Apis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="steam-api-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
