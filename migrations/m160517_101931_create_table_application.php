<?php

use yii\db\Migration;


class m160517_101931_create_table_application extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%application}}', [
            'id' => $this->primaryKey(),
            'appid' => $this->integer(),
            'name' => $this->string()->notNull(),
            'price_initial' => $this->integer()->defaultValue(null),
            'price_final' => $this->integer()->defaultValue(null),
            'discount' => $this->integer()->defaultValue(0),
            'currency_id' => $this->integer()->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'about' => $this->text()->defaultValue(null),
            'header_base' => $this->text()->defaultValue(null),
            'developer' => $this->string()->defaultValue(null),
            'publisher' => $this->string()->defaultValue(null),
            'requirements_min' => $this->text()->defaultValue(null),
            'requirements_recommended' => $this->text()->defaultValue(null),
            'legal' => $this->text()->defaultValue(null),
            'release_date' => $this->integer()->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-application-currency_id', '{{%application}}', 'currency_id');

        $this->addForeignKey('fk-application-currency_id', '{{%application}}', 'currency_id', '{{%currency}}', 'id');

    }


    public function down()
    {
        $this->dropForeignKey('fk-application-currency_id', '{{%application}}');

        $this->dropIndex('idx-application-currency_id', '{{%application}}');

        $this->dropTable('{{%application}}');
    }
}
