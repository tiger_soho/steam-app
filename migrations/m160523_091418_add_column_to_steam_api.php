<?php

use yii\db\Migration;

/**
 * Handles adding column to table `steam_api`.
 */
class m160523_091418_add_column_to_steam_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%steam_api}}', 'state', $this->integer()->defaultValue(0)->after('apikey'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%steam_api}}', 'state');
    }
}
