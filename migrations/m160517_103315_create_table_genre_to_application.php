<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_genre_to_application`.
 */
class m160517_103315_create_table_genre_to_application extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%genre_to_application}}', [
            'id' => $this->primaryKey(),
            'application_id' => $this->integer()->notNull(),
            'genre_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-genre_to_application-application_id', '{{%genre_to_application}}', 'application_id');
        $this->createIndex('idx-genre_to_application-genre_id', '{{%genre_to_application}}', 'genre_id');

        $this->addForeignKey('fk-genre_to_application-application_id', '{{%genre_to_application}}', 'application_id', '{{%application}}', 'id');
        $this->addForeignKey('fk-genre_to_application-genre_id', '{{%genre_to_application}}', 'genre_id', '{{%genre}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-genre_to_application-application_id', '{{%genre_to_application}}');
        $this->dropForeignKey('fk-genre_to_application-genre_id', '{{%genre_to_application}}');

        $this->dropIndex('idx-genre_to_application-application_id', '{{%genre_to_application}}');
        $this->dropIndex('idx-genre_to_application-genre_id', '{{%genre_to_application}}');

        $this->dropTable('{{%genre_to_application}}');
    }
}
