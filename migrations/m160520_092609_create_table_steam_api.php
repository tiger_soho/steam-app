<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_steam_api`.
 */
class m160520_092609_create_table_steam_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%steam_api}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue(null),
            'apikey' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%steam_api}}');
    }
}
