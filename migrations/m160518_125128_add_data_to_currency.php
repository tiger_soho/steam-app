<?php

use yii\db\Migration;

/**
 * Handles adding data to table `currency`.
 */
class m160518_125128_add_data_to_currency extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%currency}}', ['code'],
            [
                ['USD'],
                ['GBP'],
                ['UAH'],
                ['EUR']
            ]
        );
    }

    public function down()
    {
        $this->truncateTable('{{%currency}}');
    }
}
