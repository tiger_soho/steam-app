<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_category_to_application`.
 */
class m160517_103304_create_table_category_to_application extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category_to_application}}', [
            'id' => $this->primaryKey(),
            'application_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->createIndex('idx-category_to_application-application_id', '{{%category_to_application}}', 'application_id');
        $this->createIndex('idx-category_to_application-category_id', '{{%category_to_application}}', 'category_id');

        $this->addForeignKey('fk-category_to_application-application_id', '{{%category_to_application}}', 'application_id', '{{%application}}', 'id');
        $this->addForeignKey('fk-category_to_application-category_id', '{{%category_to_application}}', 'category_id', '{{%category}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-category_to_application-application_id', '{{%category_to_application}}');
        $this->dropForeignKey('fk-category_to_application-category_id', '{{%category_to_application}}');

        $this->dropIndex('idx-category_to_application-application_id', '{{%category_to_application}}');
        $this->dropIndex('idx-category_to_application-category_id', '{{%category_to_application}}');

        $this->dropTable('{{%category_to_application}}');
    }
}
