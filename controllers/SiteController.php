<?php

namespace app\controllers;

use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use SteamApi\App;
use app\models\Application;
use app\models\Currency;
use app\models\Category;
use app\models\Genre;
use app\models\CategoryToApplication;
use app\models\GenreToApplication;
use app\models\SteamApi;
use yii\helpers\Url;

//use Thread;

class SiteController extends Controller
{

    static $limit = 20;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionGetApps()
    {

    }

    public function getAppInfo()
    {
        //$keys = SteamApi::find()->all();
        $rows = Application::find()->count();
        //if($rows > 0) VarDumper::dump($rows);die;
        $steps = round($rows / self::$limit);
        var_dump($steps);
        $offset = 0;
        while ($steps > 0) {
            $key = self::getKey();
            if (self::getKeyState($key->id)) {
                self::setKeyState($key->id, 1);
                $pid = pcntl_fork();
                if ($pid > 0) {
                    self::getDetails($offset, self::$limit);
                }
                $offset = $offset + self::$limit;
                $steps--;
            } else {
                continue;
            }

        }
    }

    public function getKey()
    {
        return SteamApi::findOne(['state' => 0]);
    }

    public function getKeyState($id)
    {
        $state = SteamApi::findOne($id)->state;

        return ($state == 1) ? true : false;
    }

    public static function setKeyState($id, $state)
    {
        /* @var $model SteamApi */

        $model = SteamApi::findOne($id);
        $model->state = $state;
        $model->save(false);
    }

    public function getDetails($offset, $limit)
    {
        /* @var $model Application */
        $app = self::appInit();
        $apps = Application::find()->offset($offset)->limit($limit)->all();

        foreach ($apps as $item){
            $game = $app->appDetails($item->appid);
            $model = Application::findOne($item->id);
            $model->appid = $model->appid;
            $model->name = $model->name;
            $model->about = $game->about;

            $model->description = $game->description;
            try {
                $model->header_base = base64_encode($img = file_get_contents($game->header));
            } catch (ErrorException $e) {
                $model->header_base = '';
            }

            if (isset($game->pcRequirements->minimum)) {
                $model->requirements_min = $game->pcRequirements->minimum;
            }
            if (isset($game->pcRequirements->recommended)) {
                $model->requirements_recommended = $game->pcRequirements->recommended;
            }
            $model->legal = $game->legal;
            if (!empty($game->developers)) {
                $model->developer = implode(', ', $game->developers);
            }
            if (!empty($game->publishers)) {
                $model->publisher = implode(', ', $game->publishers);
            }

            if (isset($game->price->initial)) {
                $model->price_initial = $game->price->initial;
            }

            $model->price_final = $game->price->final;
            if (isset($game->price->discount_percent)) {
                $model->discount = $game->price->discount_percent;
            }
            if (isset($game->price->currency)) {
                $model->currency_id = Currency::findOne(['code' => $game->price->currency])->id;
            }

            $model->release_date = strtotime($game->release->date);
            if ($model->save()) {
                if (!empty($game->categories)) {
                    foreach ($game->categories as $category) {
                        if (empty($category_model = Category::findOne($category->id))) {
                            $category_model = new Category();
                            $category_model->id = $category->id;
                            $category_model->name = $category->description;
                            $category_model->save();
                        }
                        $category_to_app = new CategoryToApplication();
                        $category_to_app->category_id = $category->id;
                        $category_to_app->application_id = $model->id;
                        $category_to_app->save();
                    }
                }
                if (!empty($game->genres)) {
                    foreach ($game->genres as $genre) {
                        if (empty($genre_model = Genre::findOne($genre->id))) {
                            $genre_model = new Genre();
                            $genre_model->id = $genre->id;
                            $genre_model->name = $genre->description;
                            $genre_model->save();
                        }
                        $genre_to_app = new GenreToApplication();
                        $genre_to_app->genre_id = $genre->id;
                        $genre_to_app->application_id = $model->id;
                        $genre_to_app->save();
                    }
                }
            } else {
                var_dump($model->getErrors());
            }
        }
    }

    public function actionIndex()
    {
        /* @var $app App */

        $key = SteamApi::findOne(['state' => '0']);

        $app = self::appInit($key->apikey);

        $result = $app->GetAppList();

        foreach ($result as $item) {
            //VarDumper::dump(preg_replace('/\\x.*$/', '', $item->name),10,true);
            if (!Application::find()->where(['appid' => (int)$item->appid])->exists()) {
                $model = new Application();
                $model->appid = $item->appid;
                $model->name = self::removeEmoji($item->name);
                $model->save(false);
            }
        }

        self::getAppInfo();
    }

    public static function removeEmoji($text)
    {
        $cleanText = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $cleanText = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $cleanText = preg_replace($regexSymbols, '', $cleanText);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $cleanText = preg_replace($regexTransport, '', $cleanText);

        return $cleanText;
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public static function appInit($key = null)
    {
        $app = new App($key);
        return $app;
    }
}
