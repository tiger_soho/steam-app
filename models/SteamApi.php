<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%steam_api}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $apikey
 * @property integer $state
 * @property integer $created_at
 * @property integer $updated_at
 */
class SteamApi extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%steam_api}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apikey'], 'required'],
            [['state', 'created_at', 'updated_at'], 'integer'],
            [['name', 'apikey'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'apikey' => Yii::t('app', 'Apikey'),
            'state' => Yii::t('app', 'State'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
