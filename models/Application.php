<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%application}}".
 *
 * @property integer $id
 * @property integer $appid
 * @property string $name
 * @property integer $price_initial
 * @property integer $price_final
 * @property integer $discount
 * @property integer $currency_id
 * @property string $description
 * @property string $about
 * @property string $header_base
 * @property string $developer
 * @property string $publisher
 * @property string $requirements_min
 * @property string $requirements_recommended
 * @property string $legal
 * @property integer $release_date
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Currency $currency
 * @property CategoryToApplication[] $categoryToApplications
 * @property GenreToApplication[] $genreToApplications
 */
class Application extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%application}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appid', 'price_initial', 'price_final', 'discount', 'currency_id', 'release_date', 'created_at', 'updated_at'], 'integer'],
            [['name', 'currency_id'], 'required'],
            [['description', 'about', 'header_base', 'requirements_min', 'requirements_recommended', 'legal'], 'string'],
            [['name', 'developer', 'publisher'], 'string', 'max' => 255],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'appid' => Yii::t('app', 'Appid'),
            'name' => Yii::t('app', 'Name'),
            'price_initial' => Yii::t('app', 'Price Initial'),
            'price_final' => Yii::t('app', 'Price Final'),
            'discount' => Yii::t('app', 'Discount'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'description' => Yii::t('app', 'Description'),
            'about' => Yii::t('app', 'About'),
            'header_base' => Yii::t('app', 'Header Base'),
            'developer' => Yii::t('app', 'Developer'),
            'publisher' => Yii::t('app', 'Publisher'),
            'requirements_min' => Yii::t('app', 'Requirements Min'),
            'requirements_recommended' => Yii::t('app', 'Requirements Recommended'),
            'legal' => Yii::t('app', 'Legal'),
            'release_date' => Yii::t('app', 'Release Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryToApplications()
    {
        return $this->hasMany(CategoryToApplication::className(), ['application_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenreToApplications()
    {
        return $this->hasMany(GenreToApplication::className(), ['application_id' => 'id']);
    }
}
